FROM alpine:edge

COPY ./set_variables_in_CI.sh .
RUN . ./set_variables_in_CI.sh \
    && sed -i -e 's/https/http/' /etc/apk/repositories \
    # && http_proxy=$http_proxy_with_password && will not work, because the variable is immediately discarded after being set.
    # It's a feature. You don't want extraneous variables hanging around your Docker image.
    && http_proxy=$http_proxy_with_password apk add openssh-client
