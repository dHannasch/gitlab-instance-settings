{% if cookiecutter.http_proxy_with_username != 'default to none' -%}
http_proxy_with_username={{ cookiecutter.http_proxy_with_username }}
{% endif -%}
{% if cookiecutter.http_proxy_with_password != 'default to none' -%}
http_proxy_with_password={{ cookiecutter.http_proxy_with_password }}
{% endif -%}
{% if cookiecutter.proxy_host != 'default to none' -%}
proxy_host={{ cookiecutter.proxy_host }}
proxy_port={{ cookiecutter.proxy_port }}
{% endif -%}
{% if cookiecutter.http_proxy != 'default to none' -%}
{% if cookiecutter.https_proxy != 'default to none' %} {% endif %}http_proxy={{ cookiecutter.http_proxy }}
{% endif -%}
{% if cookiecutter.https_proxy != 'default to none' -%}
https_proxy={{ cookiecutter.https_proxy }}
{% endif -%}
{% if cookiecutter.ftp_proxy != 'default to none' %}  ftp_proxy={{ cookiecutter.ftp_proxy }}
{% endif -%}
{% if cookiecutter.rsync_proxy != 'default to none' -%}
rsync_proxy={{ cookiecutter.rsync_proxy }}
{% endif -%}
{% if cookiecutter.socks_proxy != 'default to none' -%}
socks_proxy={{ cookiecutter.socks_proxy }}
{% endif -%}
{% if cookiecutter.no_proxy != 'default to none' -%}
no_proxy="{{ cookiecutter.no_proxy }}"
{% endif -%}
{% if cookiecutter.repo_hosting_domain != 'gitlab.com' -%}
repo_hosting_domain="{{ cookiecutter.repo_hosting_domain }}"
{% endif -%}
{% if cookiecutter.NETWORK_SETTINGS_SCRIPT_LOCATION -%}
NETWORK_SETTINGS_SCRIPT_LOCATION="{{ cookiecutter.NETWORK_SETTINGS_SCRIPT_LOCATION }}"
{% endif -%}
{% if cookiecutter.cookiecutter_image -%}
COOKIECUTTER_IMAGE="{{ cookiecutter.cookiecutter_image }}"
{% endif -%}
{% if cookiecutter.PyPI_URL -%}
PYPI_URL="{{ cookiecutter.PyPI_URL }}"
{% endif -%}
{% if cookiecutter.Docker_Hub_URL -%}
DOCKER_HUB_URL="{{ cookiecutter.Docker_Hub_URL }}"
{% endif -%}
{% if cookiecutter.samba_server_name != 'default to none' -%}
SAMBA_SERVER_NAME="{{ cookiecutter.samba_server_name }}"
SAMBA_USERNAME_SUFFIX="{{ cookiecutter.samba_username_suffix }}"
{% endif -%}
{% if cookiecutter.OS_DEFAULT_DOMAIN != 'default' -%}
OS_DEFAULT_DOMAIN="{{ cookiecutter.OS_DEFAULT_DOMAIN }}"
{% endif -%}
{% if cookiecutter.OPENSTACK_AUTH_NET_LOC != 'default to none' -%}
OPENSTACK_AUTH_NET_LOC="{{ cookiecutter.OPENSTACK_AUTH_NET_LOC }}"
{% endif -%}
{% if cookiecutter.DNS_IPs_JSON_list != '["8.8.8.8]' -%}
DNS_IPs_JSON_list='{{ cookiecutter.DNS_IPs_JSON_list }}'
{% endif -%}
{% if cookiecutter.servers_to_whitelist_for_ssh_space_separated -%}
SERVERS_TO_WHITELIST_FOR_SSH="{{ cookiecutter.servers_to_whitelist_for_ssh_space_separated }}"
{% endif -%}
{% if cookiecutter.servers_to_whitelist_for_ssl_space_separated -%}
DOMAINS_TO_WHITELIST_FOR_SSL_SPACE_SEPARATED="{{ cookiecutter.servers_to_whitelist_for_ssl_space_separated }}"
{% endif -%}
{% if cookiecutter.base64_certificates_to_trust -%}
PROXY_CA_PEM="{{ cookiecutter.base64_certificates_to_trust }}"
{% endif -%}

