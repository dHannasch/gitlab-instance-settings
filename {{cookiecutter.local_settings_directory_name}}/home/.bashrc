{% if cookiecutter.http_proxy != 'default to none' -%}
export http_proxy={{ cookiecutter.http_proxy }}
{% endif -%}
{% if cookiecutter.https_proxy != 'default to none' -%}
export https_proxy={{ cookiecutter.https_proxy }}
{% endif -%}
{% if cookiecutter.ftp_proxy != 'default to none' %}export ftp_proxy={{ cookiecutter.ftp_proxy }}
{% endif -%}
{% if cookiecutter.rsync_proxy != 'default to none' -%}
export rsync_proxy={{ cookiecutter.rsync_proxy }}
{% endif -%}
{% if cookiecutter.socks_proxy != 'default to none' -%}
export socks_proxy={{ cookiecutter.socks_proxy }}
{% endif -%}
{% if cookiecutter.no_proxy != 'default to none' -%}
export no_proxy="{{ cookiecutter.no_proxy }}"
{% endif -%}
{% if cookiecutter.PyPI_URL -%}
export PYPI_URL="{{ cookiecutter.PyPI_URL }}"
{% endif -%}
{% if cookiecutter.samba_server_name != 'default to none' -%}
export SAMBA_SERVER_NAME="{{ cookiecutter.samba_server_name }}"
export SAMBA_USERNAME_SUFFIX="{{ cookiecutter.samba_username_suffix }}"
{% endif -%}
{% if cookiecutter.DNS_IPs_JSON_list != '["8.8.8.8]' -%}
export DNS_IPs_JSON_list='{{ cookiecutter.DNS_IPs_JSON_list }}'
{% endif -%}
{% if cookiecutter.servers_to_whitelist_for_ssh_space_separated -%}
SERVERS_TO_WHITELIST_FOR_SSH="{{ cookiecutter.servers_to_whitelist_for_ssh_space_separated }}"
{% endif -%}
{% if cookiecutter.servers_to_whitelist_for_ssl_space_separated -%}
DOMAINS_TO_WHITELIST_FOR_SSL_SPACE_SEPARATED="{{ cookiecutter.servers_to_whitelist_for_ssl_space_separated }}"
{% endif -%}
{% if cookiecutter.base64_certificates_to_trust -%}
PROXY_CA_PEM="{{ cookiecutter.base64_certificates_to_trust }}"
{% endif -%}
REQUESTS_CA_BUNDLE=$HOME/bundled.pem
