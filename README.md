
+ base64_certificates_to_trust: ne or more X.509 digital certificate files that use base64 (ASCII) encoding, such as you might find in *.pem, *.crt, *.ca-bundle, *.cer, *.p7b, or *.p7s files. If your certificates are in a binary format such as DER or PKCS#12, it may be possible to convert them.

A certificate will usually look something like this:
```
-----BEGIN CERTIFICATE-----
MIIFaDCCBFCgAwIBAgISESHkvZFwK9Qz0KsXD3x8p44aMA0GCSqGSIb3DQEBCwUA
...
BQcwAoZBaHR0cDovL3NlY3VyZS5nbG9iYWxzaWduLmNvbS9jYWNlcnQvZ3Nvcmdh
bml6YXRpb252YWxzaGEyZzJyMS5jcnQwPwYIKwYBBQUHMAGGM2h0dHA6Ly9vY3Nw
lffygD5IymCSuuDim4qB/9bh7oi37heJ4ObpBIzroPUOthbG4gv/5blW3Dc=
-----END CERTIFICATE-----
```
See more at https://comodosslstore.com/resources/what-is-a-pem-certificate/ (where that sample certificate is from).

