
import os
import os.path

import click
import ruyaml

@click.command()
@click.argument('cookiecutter_project_path_slug')
@click.argument('local_settings_directory_name')
def main(cookiecutter_project_path_slug, local_settings_directory_name):
  if not os.path.exists(local_settings_directory_name):
    raise ValueError(local_settings_directory_name)
  cookiecutterrcpath = os.path.join(local_settings_directory_name, '.cookiecutterrc')
  if not os.path.exists(cookiecutterrcpath):
    if tuple(os.listdir(local_settings_directory_name)) != ('.git',):
      raise ValueError(os.listdir(local_settings_directory_name))
  else:
    yaml = ruyaml.YAML(typ='safe')
    cookiecutterrc = yaml.load(open(cookiecutterrcpath))
    cookiecutterrc['default_context']['cookiecutter_project_path_slug'] = cookiecutter_project_path_slug
    cookiecutterrc['default_context']['local_settings_directory_name'] = 'network-settings'
    with open(cookiecutterrcpath, 'w') as rcfile:
      yaml.dump(cookiecutterrc, rcfile)

if __name__ == "__main__":
  main()

