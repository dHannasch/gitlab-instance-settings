#!/usr/bin/env sh

# SSL traditionally uses X.509 certificates for announcing server and client public keys; SSH has its own format.
echo "post_gen_project.sh"
# https://cookiecutter.readthedocs.io/en/1.7.2/advanced/hooks.html#current-working-directory
set -o allexport
. set_variables_in_CI.sh
set +o allexport

echo "We will overwrite the entire contents of git_certs.pem rather than parse it."

echo "$PROXY_CA_PEM" | tr -d '\r' > ./etc/gitlab-runner/git_certs.pem
echo "Variables set, we can install openssl."
apk add openssl || apt-get install --assume-yes openssl
echo "DOMAINS_TO_WHITELIST_FOR_SSL_SPACE_SEPARATED = $DOMAINS_TO_WHITELIST_FOR_SSL_SPACE_SEPARATED"
for DOMAIN in $DOMAINS_TO_WHITELIST_FOR_SSL_SPACE_SEPARATED; do
 echo "openssl s_client -showcerts -servername $DOMAIN -connect $DOMAIN:443"
 openssl s_client -showcerts -servername $DOMAIN -connect $DOMAIN:443 >> ./etc/gitlab-runner/git_certs.pem
done


